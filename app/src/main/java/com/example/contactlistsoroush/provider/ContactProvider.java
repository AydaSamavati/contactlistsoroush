package com.example.contactlistsoroush.provider;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import com.example.contactlistsoroush.contact.model.Contact;
import com.example.contactlistsoroush.util.StreamToByteArray;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ContactProvider {

    public static List<Contact> getContactList(Context context) {
        List<Contact> contacts = new ArrayList<>();

        Uri uri = ContactsContract.Contacts.CONTENT_URI;
        String[] projection = {
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.HAS_PHONE_NUMBER};

        String selection = null;
        String[] selectionArgs = null;
        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME;


        ContentResolver resolver = context.getContentResolver();
        Cursor contactCursor = resolver.query(uri, projection, selection, selectionArgs, sortOrder);

        if (contactCursor != null)
            while (contactCursor.moveToNext()) {

                int id = Integer.parseInt(contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts._ID)));
                String name = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                String hasPhone = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                if (hasPhone.equals("1")) {
                    Contact contact = new Contact();
                    StringBuilder numbers = new StringBuilder();
                    Cursor phoneCursor = resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                            null,
                            null);
                    if (phoneCursor != null)
                        while (phoneCursor.moveToNext()) {
                            String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            numbers.append(phoneNumber).append("\n");
                        }
                    if (phoneCursor != null)
                        phoneCursor.close();
                    contact.id = id;
                    contact.name = name;
                    contact.number = numbers.toString();
                    try {
                        InputStream inputStream = getInputStream(context, id);
                        if(inputStream != null) {
                            contact.thumbnail = StreamToByteArray.toByteArray(inputStream);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    contacts.add(contact);
                }
            }

        if (contactCursor != null)
            contactCursor.close();

        return contacts;
    }

    private static InputStream getInputStream(Context context, int id) {

        InputStream inputStream = null;

        try {
            inputStream = ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(),
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, id));

            assert inputStream != null;
            inputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return inputStream;
    }
}
