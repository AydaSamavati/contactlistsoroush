package com.example.contactlistsoroush.provider;

import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;

import com.example.contactlistsoroush.contact.worker.SyncContactWorker;

public class ContactContentObserver extends ContentObserver {

    private Context context;

    public ContactContentObserver(Handler handler, Context context) {
        super(handler);
        this.context = context;
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        super.onChange(selfChange, uri);
        if (!selfChange) {
            SyncContactWorker.enqueue(context);
        }
    }
}
