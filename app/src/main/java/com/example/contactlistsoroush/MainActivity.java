package com.example.contactlistsoroush;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.contactlistsoroush.contact.ContactAction;
import com.example.contactlistsoroush.contact.ContactSelectionInterface;
import com.example.contactlistsoroush.contact.ContactViewModel;
import com.example.contactlistsoroush.contact.adapter.ContactAdapter;
import com.example.contactlistsoroush.contact.model.Contact;

public class MainActivity extends AppCompatActivity implements ContactSelectionInterface {

    private RecyclerView recyclerView;
    private ContactAdapter contactAdapter;
    private ContactAction contactAction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recycler_view);
        initializeContactList();
        initializeContactViewModel();
        initializeContactAction();
        checkPermission();
    }



    private void checkPermission() {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 1000);
        } else {
            contactAction.initializeSync();
            contactAction.liveSyncStart();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1000) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                contactAction.initializeSync();
                contactAction.liveSyncStart();
            } else {
                Toast.makeText(this, "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private ContactAdapter getAdapter() {
        if (contactAdapter == null) {
            contactAdapter = new ContactAdapter(Glide.with(this));
            contactAdapter.setSelectionInterface(this);
        }
        return contactAdapter;
    }

    private void initializeContactAction() {
        contactAction = new ContactAction(MainActivity.this);
    }

    private void initializeContactList() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(getAdapter());
    }

    private void initializeContactViewModel() {
        ContactViewModel contactViewModel = ViewModelProviders.of(this).get(ContactViewModel.class);
        contactViewModel.getContacts().observe(this, contacts -> {
            getAdapter().setContacts(contacts);
        });
    }

    @Override
    public void onSelect(Contact contact) {
        Intent intent = new Intent(this, ContactItemActivity.class);
        intent.putExtra(ContactItemActivity.CONTACT_ID_KEY, contact.id);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        contactAction.liveSyncStop();
    }
}
