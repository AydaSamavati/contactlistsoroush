package com.example.contactlistsoroush.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;

public class JsonUtil {

    private static final String TAG = JsonUtil.class.getSimpleName();

    private static final Gson objectMapper = (new GsonBuilder()).serializeNulls().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();

    public JsonUtil() {
    }

    public static String toJson(Object object) {
        return objectMapper.toJson(object);
    }

    public static <T> T fromJson(String json, Class<T> clazz) {
        return objectMapper.fromJson(json, clazz);
    }

    public static <T> T fromJson(String json, Type type) {
        return objectMapper.fromJson(json, type);
    }

}
