package com.example.contactlistsoroush;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.contactlistsoroush.contact.ContactViewModel;
import com.example.contactlistsoroush.databinding.ActivityContactItemBinding;

public class ContactItemActivity extends AppCompatActivity {

    public static final String CONTACT_ID_KEY = "id";

    private ActivityContactItemBinding binding;
    private int contactId;
    private AppCompatImageView image;
    private RequestManager requestManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_contact_item);
        initializeContact();
        initializeContactViewModel();
    }

    private void initializeContact() {
        if (this.getIntent() != null)
            this.contactId = this.getIntent().getIntExtra(CONTACT_ID_KEY, -1);
        image = findViewById(R.id.image);
    }

    private void initializeContactViewModel() {
        ContactViewModel contactViewModel = ViewModelProviders.of(this).get(ContactViewModel.class);
        contactViewModel.getContact(contactId).observe(this, contact -> {
            binding.setContact(contact);
            requestManager = Glide.with(this);
            requestManager
                    .load(contact.thumbnail)
                    .placeholder(R.drawable.background_default_contact)
                    .error(R.drawable.background_default_contact)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(image);
        });
    }
}
