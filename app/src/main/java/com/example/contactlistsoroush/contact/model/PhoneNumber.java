package com.example.contactlistsoroush.contact.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "numbers")
public class PhoneNumber {

    @PrimaryKey()
    @ColumnInfo(name = "id")
    public int id;

    @ColumnInfo(name = "contact_id")
    public int contactId;

    @ColumnInfo(name = "number")
    public String number;

}
