package com.example.contactlistsoroush.contact;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.contactlistsoroush.contact.model.Contact;

import java.util.List;

public class ContactViewModel extends AndroidViewModel {

    private ContactRepository repository;
    private LiveData<List<Contact>> contactList;
    private LiveData<Contact> contact;

    public ContactViewModel(@NonNull Application application) {
        super(application);
        repository = new ContactRepository(application);
    }

    public LiveData<List<Contact>> getContacts() {
        if (contactList == null) {
            contactList = repository.fetchAll();
        }
        return contactList;
    }

    public LiveData<Contact> getContact(int contactId) {
        if (contact == null) {
            contact = repository.selectContact(contactId);
        }
        return contact;
    }
}
