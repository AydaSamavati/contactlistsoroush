package com.example.contactlistsoroush.contact.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.contactlistsoroush.R;
import com.example.contactlistsoroush.contact.ContactSelectionInterface;
import com.example.contactlistsoroush.contact.model.Contact;

import java.util.ArrayList;
import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyViewHolder> {

    private List<Contact> contacts = new ArrayList<>();
    private ContactSelectionInterface selectionInterface;
    private RequestManager requestManager;

    public ContactAdapter(RequestManager requestManager) {
        this.requestManager = requestManager;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Contact model = contacts.get(position);
        initializeInfo(holder, model);
        initializeClick(holder, model);
    }


    private void initializeInfo(MyViewHolder holder, Contact model) {
        holder.name.setText(model.name);
        requestManager
                .load(model.thumbnail)
                .placeholder(R.drawable.background_default_contact)
                .error(R.drawable.background_default_contact)
                .apply(RequestOptions.circleCropTransform())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.thumbnail);
    }

    private void initializeClick(MyViewHolder holder, Contact model) {
        holder.itemView.setOnClickListener(view -> {
            if (selectionInterface != null) selectionInterface.onSelect(model);
        });
    }

    public void setSelectionInterface(ContactSelectionInterface selectionInterface) {
        this.selectionInterface = selectionInterface;
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        private AppCompatTextView name;
        private AppCompatImageView thumbnail;

        private MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            thumbnail = itemView.findViewById(R.id.image);
        }
    }

}
