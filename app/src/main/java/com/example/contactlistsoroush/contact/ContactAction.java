package com.example.contactlistsoroush.contact;

import android.content.Context;
import android.os.Handler;
import android.provider.ContactsContract;

import com.example.contactlistsoroush.contact.worker.SyncContactWorker;
import com.example.contactlistsoroush.provider.ContactContentObserver;

public class ContactAction {

    private ContactContentObserver contactContentObserver;
    private Context context;

    public ContactAction(Context context) {
        this.context = context;
        this.contactContentObserver = new ContactContentObserver(new Handler(), context);
    }

    public void initializeSync(){
        SyncContactWorker.enqueue(context);
    }

    public void liveSyncStart(){
        context.getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI,
                true,
                contactContentObserver);
    }

    public void liveSyncStop(){
        context.getContentResolver().unregisterContentObserver(contactContentObserver);
    }

}
