package com.example.contactlistsoroush.contact.worker;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.annimon.stream.Stream;
import com.example.contactlistsoroush.contact.ContactRepository;
import com.example.contactlistsoroush.contact.model.Contact;
import com.example.contactlistsoroush.provider.ContactProvider;

import java.util.List;

public class SyncContactWorker extends Worker {

    private static final String TAG = SyncContactWorker.class.getSimpleName();

    public SyncContactWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }
    @NonNull
    @Override
    public Result doWork() {

        try {
            doSync(getApplicationContext());
            return Result.success();
        } catch (Exception e) {
            Log.d(TAG, "error", e);
            return Result.failure();
        }
    }

    private static void doSync(Context context) {
        ContactRepository repository = new ContactRepository(context);

        Contact[] newContact = Stream.of(ContactProvider.getContactList(context)).toArray(Contact[]::new);
        Contact[] oldContact = Stream.of(repository.all()).toArray(Contact[]::new);


        if (oldContact.length == 0) {
            repository.insert(false, newContact);
        } else {

            List<Integer> newIds = Stream.of(newContact).map(contact -> contact.id).toList();
            List<Integer> oldIds = Stream.of(oldContact).map(contact -> contact.id).toList();

            List<Contact> addContact = Stream.of(newContact).filter(i -> !oldIds.contains(i.id)).toList();
            List<Contact> deleteContact = Stream.of(oldContact).filter(i -> !newIds.contains(i.id)).toList();
            List<Contact> updateContact = Stream.of(oldContact).filter(i -> newIds.contains(i.id)).toList();

            for (Contact oContact : updateContact) {
                for (Contact nContact : newContact) {
                    if (oContact.id == (nContact.id)) {
                        oContact.name = nContact.name;
                        oContact.number = nContact.number;
                        repository.update(oContact, false);
                        break;
                    }
                }
            }

            Contact[] contacts = Stream.of(addContact).toArray(Contact[]::new);
            repository.insert(false,contacts);

            repository.delete(deleteContact);

        }

    }

    public static void enqueue(Context context) {
        OneTimeWorkRequest request = new OneTimeWorkRequest.Builder(SyncContactWorker.class)
                .build();
        WorkManager.getInstance(context).enqueue(request);
    }
}
