package com.example.contactlistsoroush.contact;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.contactlistsoroush.contact.model.Contact;

import java.util.List;

@Dao
public interface ContactDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Contact... contacts);

    @Query("SELECT * FROM Contact")
    LiveData<List<Contact>> fetchAll();

    @Query("SELECT * FROM Contact")
    List<Contact> all();

    @Query("SELECT * FROM Contact WHERE id = :contactId")
    LiveData<Contact> selectContact(int contactId);

    @Query("DELETE FROM Contact")
    void deleteAll();

    @Update
    void update(Contact address);

    @Delete
    void delete(List<Contact> addresses);
}
