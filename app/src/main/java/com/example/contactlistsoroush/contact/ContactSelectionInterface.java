package com.example.contactlistsoroush.contact;

import com.example.contactlistsoroush.contact.model.Contact;

public interface ContactSelectionInterface {
    void onSelect(Contact contact);
}
