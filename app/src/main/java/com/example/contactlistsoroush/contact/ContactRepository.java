package com.example.contactlistsoroush.contact;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.contactlistsoroush.contact.model.Contact;
import com.example.contactlistsoroush.database.AppDatabase;

import java.util.List;

public class ContactRepository {

    private ContactDAO contactDAO;

    public ContactRepository(Context context) {
        AppDatabase db = AppDatabase.getDatabase(context);
        contactDAO = db.contactDAO();
    }

    public LiveData<List<Contact>> fetchAll() {
        return contactDAO.fetchAll();
    }

    public List<Contact> all() {
        return contactDAO.all();
    }

    public LiveData<Contact> selectContact(int contactId) {
        return contactDAO.selectContact(contactId);
    }

    public void insert(boolean async, Contact... contacts) {
            if (async)
                new InsertAsyncTask(contactDAO).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, contacts);
            else contactDAO.insert(contacts);

    }

    public void delete(List<Contact> contacts) {
        contactDAO.delete(contacts);
    }

    public void clear(boolean async) {
        if (async)
            new ClearAsyncTask(contactDAO).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else contactDAO.deleteAll();
    }

    public void update(Contact contact, boolean async) {
        if (async)
            new UpdateAsyncTask(contactDAO).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, contact);
        else contactDAO.update(contact);
    }

    private static class UpdateAsyncTask extends AsyncTask<Contact, Void, Void> {

        private ContactDAO contactDAO;

        private UpdateAsyncTask(ContactDAO dao) {
            contactDAO = dao;
        }

        @Override
        protected Void doInBackground(final Contact... contacts) {
            contactDAO.update(contacts[0]);
            return null;
        }
    }

    private static class ClearAsyncTask extends AsyncTask<Void, Void, Void> {

        private ContactDAO contactDAO;

        private ClearAsyncTask(ContactDAO dao) {
            contactDAO = dao;
        }

        @Override
        protected Void doInBackground(final Void... voids) {
            contactDAO.deleteAll();
            return null;
        }
    }

    private static class InsertAsyncTask extends AsyncTask<Contact, Void, Void> {

        private ContactDAO contactDAO;

        private InsertAsyncTask(ContactDAO dao) {
            contactDAO = dao;
        }


        @Override
        protected Void doInBackground(Contact... objects) {
            contactDAO.insert(objects);
            return null;
        }
    }
}
